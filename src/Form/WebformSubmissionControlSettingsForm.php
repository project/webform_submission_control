<?php

namespace Drupal\webform_submission_control\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform_submission_control\Util\WebformSubmissioControlUtil;

/**
 * The WebformSubmissionControlSettingsForm Class.
 */
class WebformSubmissionControlSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'webform_submission_control_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['webform_submission_control.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('webform_submission_control.settings');
    $util = new WebformSubmissioControlUtil();
    $webformOptions = $util->getWebformList();
    $roles = $util->getRoleList();

    $form['webform'] = [
      '#type' => 'select',
      '#title' => $this->t('Select the webforms to control'),
      '#description' => $this->t('These webforms will be forced to submit to an entity.'),
      '#options' => !empty($webformOptions) ? $webformOptions : NULL,
      '#default_value' => $config->get('webform'),
      '#multiple' => TRUE,
    ];

    $form['role'] = [
      '#type' => 'select',
      '#title' => $this->t('Select the user roles to exclude'),
      '#description' => $this->t('These roles will not be restricted to submit these forms.'),
      '#options' => !empty($roles) ? $roles : NULL,
      '#default_value' => $config->get('role'),
      '#multiple' => TRUE,
    ];

    $form['message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message to display'),
      '#description' => $this->t('This message will be printed to prevent the user.'),
      '#default_value' => $config->get('message'),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $config = $this->config('webform_submission_control.settings');
    $config->set('webform', $form_state->getValue('webform'));
    $config->set('message', $form_state->getValue('message'));
    $config->set('role', $form_state->getValue('role'));
    $config->save();

    parent::submitForm($form, $form_state);
  }

}
