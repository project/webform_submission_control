# Webform Submission Control

The Webform Submission Control module limit webform submission to
entities, it uses a configuration form to turn down
submission of webforms which are not attached to an entity.

- Module [project page](https://drupal.org/project/webform_submission_control)

- To submit bug reports and feature suggestions, or track changes
  [issue queue](https://www.drupal.org/project/issues/webform_submission_control)


## Requirements

This module requires the following modules:

- [Webform](https://www.drupal.org/project/webform)


## Installation

- Install as you would normally install a contributed Drupal module.For further
  information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

The module has a configuration form where we select the webforms to control,
user roles to exclude for this control and
message to show to the user.

`/admin/config/webform-submission-control-settings`


## Maintainers

- Mamadou D. Diallo - [diaodiallo](https://www.drupal.org/u/diaodiallo)
- Daniel Cothran - [andileco](https://www.drupal.org/u/andileco)

**Supporting organization:**

- [John Snow, Inc. (JSI)](https://www.drupal.org/john-snow-inc-jsi)
